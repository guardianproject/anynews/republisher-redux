# republisher-redux

``` shell
mkdir logs out
poetry install
poetry run repub
```

## TODO

- [x] Offlines RSS feed xml
- [x] Downloads media and enclosures
- [x] Rewrites media urls
- [x] Image normalization (JPG, RGB)
- [x] Audio transcoding
- [x] Video transcoding
- [ ] Image compression - Do we want this?
- [x] Download and rewrite media embedded in content/CDATA fields
- [ ] Config file to drive the program
- [ ] Daemonize the program
- [ ] Operationalize with metrics and error reporting

## License

republisher-redux, a tool to mirror RSS/ATOM feeds completely offline

Copyright (C) 2024 Abel Luck

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
