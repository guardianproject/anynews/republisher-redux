from dataclasses import dataclass
from typing import Any, List


@dataclass
class ElementItem:
    feed_name: str
    el: Any
    image_urls: List[str]
    images: List[Any]
    file_urls: List[str]
    files: List[Any]
    audio_urls: List[str]
    audios: List[Any]
    video_urls: List[str]
    videos: List[Any]


@dataclass
class ChannelElementItem:
    feed_name: str
    el: Any
    image_urls: List[str]
    images: List[Any]
